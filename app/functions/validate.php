<?php

function validate(array $fields){

	$request = request();

	$validate = [];
	foreach($fields as $field => $type){
		switch($type){
		case 'string':
			$validate[$field] = filter_var($request[$field], FILTER_SANITIZE_STRING);
			break;
		case 'inteiro':
			$validate[$field] = filter_var($request[$field], FILTER_SANITIZE_NUMBER_INT);
			break;
		case 'email':
			$validate[$field] = filter_var($request[$field], FILTER_SANITIZE_EMAIL);
			break;
		}
	}

	return (object) $validate;
}

function isEmpty(){

	$request = request();

	$empty = false;
	foreach($request as $key => $value){
		if(empty($request[$key])){
			$empty = true;
		}
	}

	return $empty;
}
