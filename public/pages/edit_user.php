<?=getFlash('message');?>

<?php
$user = find('users', 'id', $_GET['id']);
?>
<form action="/pages/forms/update_user.php" method="POST" role="form">
    <div class="form-group">
        <label for="">Nome</label>
        <input type="text" class="form-control" name="user_name" value="<?=$user->name?>">
    </div>
    <input type="hidden" name="id" value="<?=$user->id?>">
    <div class="form-group">
        <label for="">Sobrenome</label>
        <input type="text" class="form-control" name="user_lastname" value="<?=$user->lastname?>">
    </div>
    <div class="form-group">
        <label for="">Email</label>
        <input type="email" class="form-control" name="user_email" value="<?=$user->email?>">
    </div>
    <button type="submit" class="btn btn-primary">Atualizar</button>
</form>