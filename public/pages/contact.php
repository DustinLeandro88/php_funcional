<h2>Contato</h2>

<?=getFlash('message')?>

<form action="/pages/forms/contact.php" method="POST" role="form">
    <div class="form-group">
        <label>Nome</label>
        <input type="text" class="form-control" name="contact_name">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" name="contact_email">
    </div>
    <div class="form-group">
        <label>Assunto</label>
        <input type="text" class="form-control" name="contact_subject">
    </div>
    <div class="form-group">
        <label>Mensagem</label>
        <textarea name="contact_message" cols="30" rows="10" class="form-control"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Enviar</button>
</form>