<?=getFlash('message');?>

<form action="/pages/forms/create_user.php" method="POST" role="form">
    <div class="form-group">
        <label for="">Nome</label>
        <input type="text" class="form-control" name="user_name">
    </div>
    <div class="form-group">
        <label for="">Sobrenome</label>
        <input type="text" class="form-control" name="user_lastname">
    </div>
    <div class="form-group">
        <label for="">Email</label>
        <input type="email" class="form-control" name="user_email">
    </div>
    <div class="form-group">
        <label for="">Password</label>
        <input type="password" class="form-control" name="user_pass">
    </div>
    <button type="submit" class="btn btn-primary">cadastrar</button>
</form>