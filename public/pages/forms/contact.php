<?php

require "../../../bootstrap.php";

if(isEmpty()){
	addFlash('message', 'Preencha todos os campos');

	return redirect("contact");
}

$validate = validate([
	'contact_name' => 'string',
	'contact_email' => 'email',
	'contact_subject' => 'string',
	'contact_message' => 'string',
]);

$data = [
	'from' => $validate->contact_email,
	'to' => 'user@domain.com.br',
	'message' => $validate->contact_message,
	'subject' => $validate->contact_subject,
];

if (send($data)) {
	addFlash('message', 'Email enviado com sucesso', 'success');

	return redirect("contact");
}