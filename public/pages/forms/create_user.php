<?php

require "../../../bootstrap.php";

if(isEmpty()){
	flash('message', 'Preencha todos os campos');

	return redirect("create_user");
}

$validate = validate([
	'user_name' => 'string',
	'user_lastname' => 'string',
	'user_email' => 'email',
	'user_pass' => 'string',
]);

$data = [
	'name' => $validate->user_name,
    'lastname' => $validate->user_name,
    'email' => $validate->user_name,
    'password' => $validate->user_name
];

$cadastrado = create('users', $data);

if($cadastrado){
	addFlash('message', 'Cadstrado com sucesso', 'success');

	return redirect('create_user');
}

addFlash('message', 'Erro ao cadastrar');
redirect('create_user');
