<?php

require "../../../bootstrap.php";

$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);

if(isEmpty()){
	addFlash('message', 'Preencha todos os campos');

	return redirect("edit_user&id=" . $id);
}

$validate = validate([
	'user_name' => 'string',
	'user_lastname' => 'string',
	'user_email' => 'email',
]);

$data = [
	'name' => $validate->user_name,
    'lastname' => $validate->user_lastname,
    'email' => $validate->user_email
];

$updated = update('users', $data, ['id', $id]);

if($updated){
	addFlash('message', 'Atualizado com sucesso', 'success');

	return redirect("edit_user&id=" . $id);
}

addFlash('message', 'Erro ao atualizar');
redirect("edit_user&id=" . $id);